package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Esta es una prueba para indigital!";
	}

	@GetMapping("/time")
	String time() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  

		LocalDateTime now = LocalDateTime.now();  

		return ("" + now);
	}
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}