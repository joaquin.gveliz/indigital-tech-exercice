resource "google_compute_network" "default" {
  name                    = var.network
  auto_create_subnetworks = "false"
}
resource "google_compute_subnetwork" "default" {
  name                     = var.subnetwork
  ip_cidr_range            = "10.125.0.0/20"
  network                  = google_compute_network.default.self_link
  region                   = var.region
  private_ip_google_access = true
}
resource "google_compute_address" "test-static-ip-address" {
  name = "static-ip-address"
}
resource "google_compute_firewall" "http-server" {
  name    = "default-allow-http"
  network = var.network

  allow {
    protocol = "tcp"
    ports    = [var.image_port]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
  depends_on = [
    google_compute_network.default,
    google_compute_subnetwork.default,
  ]
}