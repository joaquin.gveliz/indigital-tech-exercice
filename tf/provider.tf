
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.53.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.53.0"
    }
  }
  backend "gcs" {
    bucket  = "indigital-tech-exercice"
    prefix  = "prod"
  }
}

provider "google" {
  credentials = file("creds.json")
  project     = var.project_id
}
provider "google-beta" {
  credentials = file("creds.json")
  project     = var.project_id
}