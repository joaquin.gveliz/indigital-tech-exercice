FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY /target/demo-0.0.1-SNAPSHOT.jar /usr/src/app/app.jar

ENV PORT 5000
EXPOSE $PORT

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dserver.port=${PORT}","-jar","/usr/src/app/app.jar"]
